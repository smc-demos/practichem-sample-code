**NOTE: This README is old. This app was a submission for a different position in Austin 6 months ago that I declined an offer for.**


# How to run
- Start MySQL
- Create MySQL database named *sumo_survey* with username *sumo* and password *password*
- Install app dependencies
  - `npm install`
- Create / seed database
  - `npm run seed`
- Start Node.js app
  - `npm start`
- Open browser to: [http://localhost:3000/](http://localhost:3000/)
- Log into admin area using username *demo* and password *password*

# My software info
- Node: v0.12.7
- NPM: 2.11.3
- MySQL: 5.5.42

# State of the app
- Nearly every requirement (except full mobile support) was implemented in this Express 4 app, albeit due to time restrictions I wish I could have made it a bit more polished. Namely:
- I'm using Passport to provide a secure login area.
- I send a UUID and store on client as cookie if none exists. This is sent when a user submits a survey response. Users will see a random, unsubmitted survey each load (note that this means you can hit refresh and cycle through lots of unsubmitted ones, which I thought conformed to requirements).
- I tested on latest versions of Chrome, Safari, and Firefox.
- I am completely new to SequelizeJS, so I had to drop down to raw SQL one time and use two chained queries another time (where I wanted a subquery) due to unfamiliarity with how to achieve the functionality via the API.

# Spent 15-20 hours on this, wish I had time for:
- Finishing up the few remaining TODOs and Notes (like using Mustache-style templates to make a couple portions of views DRY and less JS-stringy... I picked EJS for a layout engine and didn't know of clean way to achieve it offhand).
- Finding an Express 4 reverse router for URLs.
- Implementing all the CRUD operations for Surveys.
- Adding automated testing (yeah, about that... sorry, I know not cool).
- Dockerizing the app to make it easier for you all to test it.
- Making the survey results more visually appealing (like pie/bar charts) instead of text percentages.
- UI updates: a) making the add survey screen work better on mobile (most other screens look okay), and b) making the UI a bit more fluid in terms of space used on desktop browsers.

### Unfortunately, I had heavier that normal work and family commitments that competed for my attention this past week. However, I still wanted to limit myself to 7 days from the first commit. I hope that a few 1am and 5am checkins show I did my best to make a solid effort. :-) Look forward to hearing from you.