var db = require('./config/database');
var sequelize = db.sequelize;
var crypto = require('crypto');

var User = db.models.User;
var Survey = db.models.Survey;
var SurveyChoice = db.models.SurveyChoice;
var Response = db.models.Response;

// Quick and dirty one shot "migration" :)
// Would use proper migrations in production
sequelize.sync({
  force: true
}).then(function() {
  return User.create({
    username: 'demo',
    password: crypto.createHash('md5').update('password').digest('hex')
  });
}).then(function(user) {
  return Survey.create({
    title: 'What do you like most about Shayne?',
    UserId: user.id,
    SurveyChoices: [{
      name: 'His hilarious sense of humor'
    }, {
      name: 'You know like his nunchuck skills... bowhunting skills... computer hacking skills...'
    }, {
      name: 'Getting lost in his hazel green eyes'
    }]
  }, {
    include: [SurveyChoice]
  }).then(function() {
    return Survey.create({
      title: 'Which academic outcome is more impressive?',
      UserId: user.id,
      SurveyChoices: [{
        name: 'Successfully completing a Master\'s degree in CS from UIUC'
      }, {
        name: 'Dropping out of UT Austin\'s PhD program like a boss'
      }]
    }, {
      include: [SurveyChoice]
    });
  }).then(function() {
    return Survey.create({
      title: 'Shayne was a software engineer at Morgan Stanley in the credit derivatives group during 2008 great recession- coincidence?',
      UserId: user.id,
      SurveyChoices: [{
        name: 'Yes'
      }, {
        name: 'No ("Excellent..." in Mr. Burns voice)'
      }]
    }, {
      include: [SurveyChoice]
    });
  });
}).then(function() {
  process.exit();
});
