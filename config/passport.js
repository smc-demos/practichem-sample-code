var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var crypto = require('crypto');
var User = require('./database').models.User;

module.exports = {
  init: function() {
    // Passport auth, inspiration: https://gist.github.com/langhard/6790602
    passport.use(new LocalStrategy(
      function(username, password, done) {
        User.find({
          where: {
            username: username,
            password: crypto.createHash('md5').update(password).digest('hex')
          }
        }).then(function(user) {
          if (user !== null) {
            return done(null, user);
          } else {
            return done(null, false, {
              message: 'Invalid login attempt'
            });
          }
        });
      }
    ));

    // Serialized and deserialized methods when got from session
    passport.serializeUser(function(user, done) {
      done(null, user);
    });
    passport.deserializeUser(function(user, done) {
      done(null, user);
    });
  }
};
