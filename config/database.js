var Sequelize = require('Sequelize');

// Note: Would use environment variables in production
var sequelize = new Sequelize('sumo_survey', 'sumo', 'password', {
  host: 'localhost',
  dialect: 'mysql',
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  }
});

var Survey = require('../models/survey')(sequelize);
var SurveyChoice = require('../models/survey_choice')(sequelize);
var User = require('../models/user')(sequelize);
var Response = require('../models/response')(sequelize);

// Set up associations
SurveyChoice.belongsTo(Survey);
Survey.hasMany(SurveyChoice);

Survey.belongsTo(User);
User.hasMany(Survey);

Response.belongsTo(Survey);
Survey.hasMany(Response);
Response.belongsTo(SurveyChoice);
SurveyChoice.hasMany(Response);

module.exports = {
  init: function() {
    // Note: Would use Sequelize migrations in production
    sequelize.sync();
  },
  models: {
    User: User,
    Survey: Survey,
    SurveyChoice: SurveyChoice,
    Response: Response
  },
  sequelize: sequelize
};
