var Sequelize = require('Sequelize');

module.exports = function(sequelize) {
  var Survey = sequelize.define('Survey', {
    title: {
      type: Sequelize.STRING(1024),
      validate: {
        notEmpty: {
          msg: 'Title is required'
        },
        len: {
          args: [1, 1024],
          msg: 'Max character count for Title is 1024'
        }
      }
    }
  });

  return Survey;
};
