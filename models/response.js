var Sequelize = require('Sequelize');

module.exports = function(sequelize) {
  var Response = sequelize.define('Response', {
    ClientUUID: {
      type: Sequelize.STRING,
      unique: 'compositeIndex',
      validate: {
        notEmpty: true
      }
    },
    SurveyId: {
      type: Sequelize.INTEGER,
      unique: 'compositeIndex'
    }
  });

  return Response;
};
