var Sequelize = require('Sequelize');

module.exports = function(sequelize) {
  var SurveyChoice = sequelize.define('SurveyChoice', {
    name: {
      type: Sequelize.STRING(2048),
      validate: {
        notEmpty: {
          msg: 'Choice text is required'
        },
        len: {
          args: [1, 2048],
          msg: 'Max character count for a Choice is 2048'
        }
      }
    }
  });

  return SurveyChoice;
};
