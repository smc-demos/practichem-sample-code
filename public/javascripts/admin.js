$(document).ready(function() {
  function displayAlert(str, type) {
    // TODO: use template instead
    var msg = $('<div class="alert alert-' + type + '">' + str + '</div>').delay(5000).fadeOut(400);
    $('#alert-area').empty();
    msg.appendTo('#alert-area');
  }

  function displayError(str) {
    displayAlert(str, 'error');
  }

  function displaySuccess(str) {
    displayAlert(str, 'success');
  }

  var cid = 0;

  function addChoice() {
    // TODO: use template instead
    cid++;
    $('#choices-list').append('<tr class="delete-choice">\
        <td><div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">\
          <input style="display: inline-block" class="mdl-textfield__input choice-input" type="text" id="b' + cid + '" />\
          <label class="mdl-textfield__label" for="b' + cid + '">Choice</label>\
        </div></td>\
        <td><button class="remove-choice mdl-button mdl-js-button mdl-js-ripple-effect" tabindex="-1">X</button></td>\
      </tr>');
    // Google MDL dynamic class stuff
    componentHandler.upgradeElement($('input#b' + cid).parent()[0]);
  }

  // Form nicety to prevent enter from doing damage
  $(window).keydown(function(event) {
    if (event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });

  $('#choices-list').on('click', 'button', function(event) {
    var numChoices = $('#choices-list .choice-input').length

    if (numChoices < 3) {
      displayError('You must have at least 2 choices');
    } else {
      $(this).closest('.delete-choice').remove()
    }

    event.preventDefault();
  });

  // start with 2 choices
  addChoice();
  addChoice();

  $('#add-choice').click(function(event) {
    addChoice();

    event.preventDefault();
  });

  $('form#new-survey').submit(function(event) {
    var choices = $('.choice-input').map(function() {
      return $(this).val();
    }).toArray();

    var formData = {
      'title': $('input[name=title]').val(),
      'choices[]': choices
    };

    $.ajax({
      'type': 'POST',
      'url': '/admin/surveys',
      'data': formData,
      'dataType': 'json',
      'encode': true
    }).error(function(data) {
      displayError(data.responseJSON.message);
    }).success(function(data) {
      // TODO: use template instead
      $('#survey-list').prepend('<li><a href="/admin/surveys/' + data.id + '">' + data.title + '</a></li>');
      $('.mdl-textfield__input').val('').parent().removeClass('is-dirty');
      displaySuccess('Created survey: ' + data.title);
    });

    event.preventDefault();
  });
});
