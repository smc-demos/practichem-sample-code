var express = require('express');
var ejsLayouts = require('express-ejs-layouts');
var flash = require('connect-flash');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var uuid = require('node-uuid');

require('./config/database').init();
require('./config/passport').init();

var app = express();

// view engine setup
app.use(ejsLayouts);
app.set('layout extractScripts', true);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(require('express-session')({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(passport.initialize());
app.use(passport.session());

// flash message support
app.use(flash());
app.use(function(req, res, next) {
  res.locals.success = req.flash('success');
  res.locals.errors = req.flash('error');
  next();
});

// helps us determine what surveys to show visitors
app.use(function(req, res, next) {
  var cookie = req.cookies.clientViewId;
  if (cookie === undefined) {
    res.cookie('clientViewId', uuid.v4(), {
      maxAge: 31536000000 // 1 year
    });
  }
  next();
});

// include passport user implicity for each template
app.use(function(req, res, next) {
  res.locals.user = req.user;
  next();
});

app.use('/', require('./routes/index'));
app.use('/admin', require('./routes/admin'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;
