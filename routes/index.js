var express = require('express');
var router = express.Router();
var passport = require('passport');
var _ = require('lodash');

var sequelize = require('../config/database').sequelize;
var Survey = require('../config/database').models.Survey;
var SurveyChoice = require('../config/database').models.SurveyChoice;
var Response = require('../config/database').models.Response;

router.get('/', function(req, res) {
  // Note: wish I knew of a Sequelize way to have $notIn subqueries.
  // The two query approach below is not a scalable solution in cases when users
  // could respond to lots of surveys.
  Response.findAll({
    attributes: ['SurveyId'],
    where: {
      ClientUUID: req.cookies.clientViewId
    }
  }).then(function(seenResponses) {
    var query = {
      order: [
        [SurveyChoice, 'id'],
        [sequelize.fn('RAND')]
      ],
      include: [{
        model: SurveyChoice
      }]
    };

    var seenResponseIds = _.map(seenResponses, function(r) {
      return r.SurveyId;
    });

    // Restrict candidate surveys this way to prevent NOT IN (NULL) queries
    // that will always return false.
    if (seenResponseIds.length > 0) {
      query.where = {
        id: {
          $notIn: seenResponseIds
        }
      };
    }

    return Survey.findOne(query);
  }).then(function(survey) {
    res.render('index', {
      title: 'Sumo Survey - Home',
      user: req.user,
      survey: survey
    });
  });
});

router.get('/login',
  function(req, res) {
    res.render('login', {
      title: 'Sumo Survey - Admin login'
    });
  });

router.post('/login',
  passport.authenticate('local', {
    failureRedirect: '/login',
    failureFlash: true
  }),
  function(req, res) {
    res.redirect('/admin');
  });

router.get('/logout',
  function(req, res) {
    req.logout();
    res.redirect('/');
  });

router.post('/saveResponse',
  function(req, res) {
    if (req.cookies.clientViewId && req.body.survey_id && req.body.choice_id) {
      Response.create({
        ClientUUID: req.cookies.clientViewId,
        SurveyId: req.body.survey_id,
        SurveyChoiceId: req.body.choice_id
      }).then(function() {
        res.redirect('/');
      });
    } else {
      req.flash('error', 'Submission not recorded due to missing input(s)');
      res.redirect('/');
    }
  });

module.exports = router;
