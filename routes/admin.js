var express = require('express');
var router = express.Router();
var _ = require('lodash');

var db = require('../config/database');
var sequelize = db.sequelize;
var Survey = db.models.Survey;
var SurveyChoice = db.models.SurveyChoice;

// Middleware function to be used for every secured route
var auth = function(req, res, next) {
  if (!req.isAuthenticated()) {
    req.flash('error', 'You must be logged in to view that page');
    res.redirect('/');
  } else {
    next();
  }
};

// Secured routes
router.get('/', auth, function(req, res) {
  Survey.findAll({
    where: {
      UserId: req.user.id
    },
    order: 'createdAt DESC'
  }).then(function(surveys) {
    res.render('admin', {
      title: 'Sumo Survey - Your surveys',
      surveys: surveys
    });
  });
});

router.post('/surveys', auth, function(req, res) {
  var title = req.body.title;
  var choices = req.body['choices[]'];

  // Quick fail checks here, model validations are final say
  if (title.length > 0 && _.isArray(choices) && choices.length >= 2 &&
    _.every(choices, function(c) {
      return c.length > 0;
    })) {
    Survey.create({
      'title': title,
      'UserId': req.user.id,
      SurveyChoices: _.map(choices, function(choice) {
        return {
          name: choice
        };
      })
    }, {
      include: [SurveyChoice]
    }).then(function(survey) {
      res.status(201).json(survey);
    }).catch(function(errors) {
      res.status(400).json(errors);
    });
  } else {
    res.status(400).json({
      'message': 'Missing required field(s)'
    });
  }
});

router.get('/surveys/:id', auth, function(req, res) {
  // Note: I was fighting Sequelize far too long to do this, so dropped
  // down to raw SQL.
  sequelize.query("SELECT \
    Surveys.title, \
    SurveyChoices.id, \
    SurveyChoices.name, \
    COUNT(Responses.id) AS ResponseCount \
  FROM \
    SurveyChoices \
    LEFT JOIN Responses ON SurveyChoices.id = Responses.SurveyChoiceId \
    LEFT JOIN Surveys ON SurveyChoices.SurveyId = Surveys.id \
  WHERE \
    SurveyChoices.SurveyId = ? AND \
    Surveys.UserId = ? \
  GROUP BY\ SurveyChoices.id ", {
    replacements: [req.params.id, req.user.id],
    type: sequelize.QueryTypes.SELECT
  }).then(function(results) {
    if (results.length > 0) {
      res.render('results', {
        title: 'Sumo Survey - Survey results',
        results: results,
        totalResponses: _.sum(results, function(r) {
          return r.ResponseCount;
        })
      });
    } else {
      res.status(400).json({
        'message': 'No results found'
      });
    }
  });
});

module.exports = router;
